FROM docker.elastic.co/beats/metricbeat:7.5.1
USER root
COPY --chown=root:metricbeat ./config/metricbeat/metricbeat.yml /usr/share/metricbeat/metricbeat.yml
RUN chmod go-w /usr/share/metricbeat/metricbeat.yml
USER metricbeat
