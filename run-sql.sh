template_dir="$PWD"
running_major_dir="$template_dir/sql/running-major.conf"
running_dir="$template_dir/sql/running.conf"
server_dir="$template_dir/sql/servers.json"
mssql_major_dir="$template_dir/config/logstash/pipeline/sql/logstash-mssql-major.conf"
mongodb_major_dir="$template_dir/config/logstash/pipeline/sql/logstash-mongodb-major.conf"
mssql_dir="$template_dir/config/logstash/pipeline/sql/logstash-mssql.conf"
mongodb_dir="$template_dir/config/logstash/pipeline/sql/logstash-mongodb.conf"
mssql_compose_dir="$template_dir/docker-compose-mssql.yml"
mongodb_compose_dir="$template_dir/docker-compose-mongodb.yml"
error_dir="$template_dir/sql/error.json"

echo $template_dir

# remove file config
rm -rf $mssql_dir
rm -rf $mongodb_dir

# get info had run
element=$(cat $running_dir)
length=$(jq -r ".servers | length" $server_dir)
type_ran=$(jq -r ".servers[$element].type" $server_dir)

# remove old container
if [ $type_ran == "mssql" ];
then
    echo "remove old mssql"
    result=$(docker logs --tail 2 logstash-mssql);
    if grep -q "Logstash shut down" <<< "$result"; then
        echo "success"
    else
        id_old=$(jq -r ".servers[$element].id" $server_dir)
        echo $id_old >> $error_dir
    fi
    docker stop logstash-mssql
    docker rm logstash-mssql
elif [ $type_ran == "mongodb" ];
then
    echo "remove old mongodb"
    result=$(docker logs --tail 2 logstash-mongodb);
    if grep -q "Logstash shut down" <<< "$result"; then
        echo "success"
    else
        id_old=$(jq -r ".servers[$element].id" $server_dir)
        echo $id_old >> $error_dir
    fi
    docker stop logstash-mongodb
    docker rm logstash-mongodb
fi

# get currently element
((element=$element + 1))
if [ $element -eq $length ];
then
    element=0
fi

id=$(jq -r ".servers[$element].id" $server_dir)
type=$(jq -r ".servers[$element].type" $server_dir)
ip=$(jq -r ".servers[$element].ip" $server_dir)

# run container
if [ $type == "mssql"  ];
then
    echo "run mssql"
    cp $mssql_major_dir $mssql_dir
    sed -i "s/%%ID%%/$id/g" $mssql_dir
    sed -i "s/%%IP%%/$ip/g" $mssql_dir
    cp $running_major_dir $running_dir
    sed -i "s/%%ELEMENT%%/$element/g" $running_dir
    docker-compose -f $mssql_compose_dir up -d
elif [ $type == "mongodb" ];
then
    echo "run mongodb"
    cp $mongodb_major_dir $mongodb_dir
    sed -i "s/%%ID%%/$id/g" $mongodb_dir
    sed -i "s/%%IP%%/$ip/g" $mongodb_dir
    cp $running_major_dir $running_dir
    sed -i "s/%%ELEMENT%%/$element/g" $running_dir
    docker-compose -f $mongodb_compose_dir up -d
fi
