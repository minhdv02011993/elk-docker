template_dir="$PWD"
running_major_dir="$template_dir/sql/running-major.conf"
running_dir="$template_dir/sql/running.conf"
server_dir="$template_dir/sql/servers.json"
mssql_major_dir="$template_dir/config/logstash/pipeline/sql/logstash-mssql-major.conf"
mssql_dir="$template_dir/config/logstash/pipeline/sql/logstash-mssql.conf"
mssql_input_major_dir="$template_dir/config/logstash/pipeline/sql/logstash-mssql-input-major.conf"
mssql_compose_dir="$template_dir/docker-compose-mssql.yml"
error_dir="$template_dir/sql/error.json"
running_number=$(jq -r ".running_number" $server_dir)
# remove file config
rm -rf $mssql_dir

# get info had run
element=$(cat $running_dir)
length=$(jq -r ".servers | length" $server_dir)

#consider container shutdown/running  remove old container
echo "remove old logstash mssql"
result=$(docker logs --tail 2 logstash-mssql)
if grep -q "Logstash shut down" <<< "$result"; then
    echo "success"
else
    echo $element >> $error_dir
fi
docker stop logstash-mssql
docker rm logstash-mssql

# replace number jdbc
rm -rf $mssql_dir
cp $mssql_major_dir $mssql_dir
for (( counter=1; counter<=running_number; counter++  ))
do
    # get currently element
    ((element+=1))
    echo $element
    if [ $element -ge $length  ];
    then
        element=0
    fi
    id=$(jq -r ".servers[$element].id" $server_dir)
    ip=$(jq -r ".servers[$element].ip" $server_dir)
    # replace input jdbc
    if [ $counter -eq $running_number  ];
    then
        sed -i -e "/%%JDBC%%/r $mssql_input_major_dir" -e "//d" $mssql_dir
        sed -i "s/%%ID%%/$id/g" $mssql_dir
        sed -i "s/%%IP%%/$ip/g" $mssql_dir
        cp $running_major_dir $running_dir
        sed -i "s/%%ELEMENT%%/$element/g" $running_dir
    else
        sed -i -e "/%%JDBC%%/r $mssql_input_major_dir"  $mssql_dir
        sed -i "s/%%ID%%/$id/g" $mssql_dir
        sed -i "s/%%IP%%/$ip/g" $mssql_dir
    fi

done

# run container
docker-compose -f $mssql_compose_dir up -d

