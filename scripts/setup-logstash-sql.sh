#!/bin/bash

es_url=http://elasticsearch:9200
path_file_mssql_block_process_report="/usr/share/logstash/config/mssql-block-process-report-template.json"
path_file_mssql_slow_query_log="/usr/share/logstash/config/mssql-slow-query-log-template.json"
path_file_mongodb_order_tracking="/usr/share/logstash/config/mongodb-order-tracking-template.json"
path_file_policy="/usr/share/logstash/config/sql-policy.json"
path_file_mongodb_sync="/usr/share/logstash/config/mongodb-sync-template.json"

echo "=== Create template mssql block process report ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
      -XPUT $es_url/_template/mssql-block-process-report-template \
      -d @$path_file_mssql_block_process_report
do
    sleep 2
    echo "Retrying create template mssql block process report..."
done

echo "=== Create template mssql slow query log ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
      -XPUT $es_url/_template/mssql-slow-query-log-template \
      -d @$path_file_mssql_slow_query_log
do
    sleep 2
    echo "Retrying create template mssql slow query log..."
done

echo "=== Create template mongodb order tracking ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
        -XPUT $es_url/_template/mongodb-order-tracking-template \
        -d @$path_file_mongodb_order_tracking
do
    sleep 2
    echo "Retrying create template mongodb order tracking..."
done

echo "=== Create template mongodb sync ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
            -XPUT $es_url/_template/mongodb-sync-template \
            -d @$path_file_mongodb_sync
do
    sleep 2
    echo "Retrying create template mongodb sync..."
done


echo "=== Create life cycle index ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
    -XPUT $es_url/_ilm/policy/sql_policy \
    -d @$path_file_policy
do
    sleep 2
    echo "Retrying create policy"
done

echo "=== Start logstash ==="
/usr/local/bin/docker-entrypoint
