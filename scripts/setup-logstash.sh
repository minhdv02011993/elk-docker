#!/bin/bash

es_url=http://elasticsearch:9200
path_file_policy="/usr/share/logstash/config/my-policy.json"
path_file_ha="/usr/share/logstash/config/haproxy-template.json"
path_file_pb="/usr/share/logstash/config/packetbeat-template.json"
path_file_error="/usr/share/logstash/config/error-template.json"
path_file_service="/usr/share/logstash/config/service-template.json"
path_file_notification="/usr/share/logstash/config/notification_service-template.json"
path_file_fnbrealtimedb="/usr/share/logstash/config/fnbrealtimedb-template.json"
path_file_tracking="/usr/share/logstash/config/tracking-template.json"
path_file_omniChannel="/usr/share/logstash/config/omniChannel-template.json"
path_file_kvauth="/usr/share/logstash/config/kv-auth-template.json"
path_file_kvtimesheet="/usr/share/logstash/config/kv-timesheet-template.json"
path_file_shipservice="/usr/share/logstash/config/shipservice-template.json"
path_file_webhook="/usr/share/logstash/config/webhook-template.json"
path_file_importexport="/usr/share/logstash/config/importexport-template.json"
path_file_fbservice="/usr/share/logstash/config/fbservice-template.json"
path_file_monitorservice="/usr/share/logstash/config/monitorservice-template.json"
path_file_redis="/usr/share/logstash/config/redis-template.json"
path_file_rabbitmq="/usr/share/logstash/config/rabbitmq-template.json"
path_file_kafka="/usr/share/logstash/config/kafka-template.json"
path_file_mongodb="/usr/share/logstash/config/mongodb-template.json"
path_file_mssql="/usr/share/logstash/config/mssql-template.json"
path_file_haproxy_check="/usr/share/logstash/config/haproxy-check-template.json"

echo "=== Create template haproxy ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
      -XPUT $es_url/_template/haproxy-template \
      -d @$path_file_ha
do
    sleep 2
    echo "Retrying create template haproxy..."
done

echo "=== Create template haproxy check ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
    -XPUT $es_url/_template/haproxy-check-template \
    -d @$path_file_haproxy_check
do
    sleep 2
    echo "Retrying create template haproxy check..."
done

echo "=== Create template packetbeat ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
      -XPUT $es_url/_template/packetbeat-template \
      -d @$path_file_pb
do
    sleep 2
    echo "Retrying create template packetbeat..."
done

echo "=== Create template error ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
      -XPUT $es_url/_template/error-template \
      -d @$path_file_error
do
    sleep 2
    echo "Retrying create template error..."
done

echo "=== Create template tracking ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
      -XPUT $es_url/_template/tracking-template \
      -d @$path_file_tracking
do
    sleep 2
    echo "Retrying create template tracking..."
done


echo "=== Create template service ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
      -XPUT $es_url/_template/service-template \
      -d @$path_file_service
do
    sleep 2
    echo "Retrying create template service..."
done

echo "=== Create template notification service ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
      -XPUT $es_url/_template/notification_service-template \
      -d @$path_file_notification
do
    sleep 2
    echo "Retrying create template notification service..."
done

echo "=== Create template fnbrealtimedb ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
      -XPUT $es_url/_template/fnbrealtimedb-template \
      -d @$path_file_fnbrealtimedb
do
    sleep 2
    echo "Retrying create template fnbrealtimedb..."
done

echo "=== Create template OmniChannel ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
      -XPUT $es_url/_template/omni-channel-template \
      -d @$path_file_omniChannel
do
    sleep 2
    echo "Retrying create template omniChannel..."
done

echo "=== Create template KvAuth ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
      -XPUT $es_url/_template/kv-auth-template \
      -d @$path_file_kvauth
do
    sleep 2
    echo "Retrying create template kvAuth..."
done

echo "=== Create template KvTimesheet ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
      -XPUT $es_url/_template/kv-timesheet-template \
      -d @$path_file_kvtimesheet
do
    sleep 2
    echo "Retrying create template kvtimesheet..."
done

echo "=== Create template shipservice ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
      -XPUT $es_url/_template/shipservice-template \
      -d @$path_file_shipservice
do
    sleep 2
    echo "Retrying create template shipservice..."
done

echo "=== Create template webhook ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
      -XPUT $es_url/_template/webhook-template \
      -d @$path_file_webhook
do
    sleep 2
    echo "Retrying create template webhook..."
done


echo "=== Create template importexport ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
      -XPUT $es_url/_template/importexport-template \
      -d @$path_file_importexport
do
    sleep 2
    echo "Retrying create template importexport..."
done


echo "=== Create template fbservice ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
      -XPUT $es_url/_template/fbservice-template \
      -d @$path_file_fbservice
do
    sleep 2
    echo "Retrying create template fbservice..."
done

echo "=== Create template monitorservice ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
      -XPUT $es_url/_template/monitorservice-template \
      -d @$path_file_monitorservice
do
    sleep 2
    echo "Retrying create template monitorservice..."
done

echo "=== Create template redis ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
        -XPUT $es_url/_template/redis-template \
        -d @$path_file_redis
do
    sleep 2
    echo "Retrying create template redis..."
done

echo "=== Create template rabbitmq ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
        -XPUT $es_url/_template/rabbitmq-template \
        -d @$path_file_rabbitmq
do
    sleep 2
    echo "Retrying create template rabbitmq..."
done

echo "=== Create template kafka ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
        -XPUT $es_url/_template/kafka-template \
        -d @$path_file_kafka
do
        sleep 2
        echo "Retrying create template kafka..."
done

echo "=== Create template mongodb ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
        -XPUT $es_url/_template/mongodb-template \
        -d @$path_file_mongodb
do
        sleep 2
        echo "Retrying create template mongodb..."
done

echo "=== Create template mssql ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
        -XPUT $es_url/_template/mssql-template \
        -d @$path_file_mssql
do
        sleep 2
        echo "Retrying create template mssql..."
done


echo "=== Create life cycle index ==="
until curl -u "elastic:${ELASTIC_PASSWORD}" -s -H 'Content-Type:application/json' \
    -XPUT $es_url/_ilm/policy/my_policy \
    -d @$path_file_policy
do
    sleep 2
    echo "Retrying create policy"
done

echo "=== Start logstash ==="
/usr/local/bin/docker-entrypoint
