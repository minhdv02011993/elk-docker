template_dir="$PWD"
running_major_dir="$template_dir/sql/running-major-mongodb.conf"
running_dir="$template_dir/sql/running-mongodb.conf"
server_dir="$template_dir/sql/servers-mongodb.json"
mongodb_major_dir="$template_dir/config/logstash/pipeline/sql/logstash-mongodb-major.conf"
mongodb_dir="$template_dir/config/logstash/pipeline/sql/logstash-mongodb.conf"
mongodb_input_major_dir="$template_dir/config/logstash/pipeline/sql/logstash-mongodb-input-major.conf"
mongodb_compose_dir="$template_dir/docker-compose-mongodb.yml"
error_dir="$template_dir/sql/error-mongodb.json"
running_number=$(jq -r ".running_number" $server_dir)
# remove file config
rm -rf $mongodb_dir

# get info had run
element=$(cat $running_dir)
length=$(jq -r ".servers | length" $server_dir)
echo $element
echo $length

#consider container shutdown/running  remove old container
echo "remove old logstash mongodb"
result=$(docker logs --tail 2 logstash-mongodb)
if grep -q "Logstash shut down" <<< "$result"; then
    echo "success"
else
    echo $element >> $error_dir
fi
docker stop logstash-mongodb
docker rm logstash-mongodb

# replace number jdbc
rm -rf $mongodb_dir
cp $mongodb_major_dir $mongodb_dir
for (( counter=1; counter<=running_number; counter++  ))
do
    # get currently element
    ((element+=1))
    echo $element
    if [ $element -ge $length  ];
    then
        element=0
    fi
    id=$(jq -r ".servers[$element].id" $server_dir)
    ip=$(jq -r ".servers[$element].ip" $server_dir)
    table=$(jq -r ".servers[$element].table"  $server_dir)
    query=$(jq -r ".servers[$element].query" $server_dir)

    # replace input jdbc
    if [ $counter -eq $running_number  ];
    then
        sed -i -e "/%%JDBC%%/r $mongodb_input_major_dir" -e "//d" $mongodb_dir
        sed -i "s/%%ID%%/$id/g" $mongodb_dir
        sed -i "s/%%IP%%/$ip/g" $mongodb_dir
        sed -i "s/%%TABLE%%/$table/g" $mongodb_dir
        sed -i "s/%%QUERY%%/$query/g" $mongodb_dir
        cp $running_major_dir $running_dir
        sed -i "s/%%ELEMENT%%/$element/g" $running_dir
    else
        sed -i -e "/%%JDBC%%/r $mongodb_input_major_dir"  $mongodb_dir
        sed -i "s/%%ID%%/$id/g" $mongodb_dir
        sed -i "s/%%IP%%/$ip/g" $mongodb_dir
        sed -i "s/%%TABLE%%/$table/g" $mongodb_dir
        sed -i "s/%%QUERY%%/$query/g" $mongodb_dir
    fi

done

# run container
docker-compose -f $mongodb_compose_dir up -d

